// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyANIUfg7WxI4Z7VqTiVaS5mmBLdyxNgSl4",
  authDomain: "simple-notelist-vue.firebaseapp.com",
  projectId: "simple-notelist-vue",
  storageBucket: "simple-notelist-vue.appspot.com",
  messagingSenderId: "1023428555345",
  appId: "1:1023428555345:web:76bc8124db6966e0e63e70",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export { db, auth };
