import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
import { useAuthStore } from "./stores/AuthStore";

const pinia = createPinia();
const app = createApp(App);

router.beforeEach((to, from) => {
  const authStore = useAuthStore();

  const isAuthenticated = authStore.user.uid ? true : false;

  if (isAuthenticated && to.name === "Auth") {
    return { name: "Notes" };
  }

  if (!isAuthenticated && to.name !== "Auth") {
    return { name: "Auth" };
  }
});

app.use(router);
app.use(pinia);

app.mount("#app");
