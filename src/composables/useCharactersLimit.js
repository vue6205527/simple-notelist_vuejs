import { watch } from "vue";

export function useCharactersLimit(value, limit) {
  watch(value, (newValue, oldValue) => {
    if (newValue.length === limit) {
      alert(`Sorry!! More than ${limit} characters are not allowed`);
    }
  });
}
