import { createRouter, createWebHashHistory } from "vue-router";

import NotesView from "../views/NotesView.vue";
import StatesView from "../views/StatesView.vue";
import EditNoteView from "../views/EditNoteView.vue";
import AuthView from "../views/AuthView.vue";

const routes = [
  {
    name: "Notes",
    path: "/",
    component: NotesView,
  },
  {
    name: "Auth",
    path: "/auth",
    component: AuthView,
  },
  {
    name: "EditNote",
    path: "/edit-note/:id",
    component: EditNoteView,
  },
  {
    name: "States",
    path: "/states",
    component: StatesView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
