import { defineStore } from "pinia";
import { ref, computed } from "vue";
import { collection, query, orderBy, doc, onSnapshot, addDoc, updateDoc, deleteDoc } from "firebase/firestore";

import { db } from "../js/firebase";
import { useAuthStore } from "./AuthStore";

export const useNotesStore = defineStore("notesStore", () => {
  const notes = ref([]);
  const notesLoaded = ref(false);

  const authStore = useAuthStore();

  let notesCollectionRef;
  let notesCollectionQuery;
  let notesSnapshot = null;

  const getNoteContentById = computed(() => {
    return (id) => {
      return notes.value.find((note) => note.id === id).content;
    };
  });

  const totalNotesCount = computed(() => {
    return notes.value.length;
  });

  const totalCharactersCount = computed(() => {
    return notes.value.reduce((total, note) => total + note.content.length, 0);
  });

  async function getNotes(userUid) {
    notesCollectionRef = collection(db, "users", userUid, "notes");
    notesCollectionQuery = query(notesCollectionRef, orderBy("date", "desc"));

    notesSnapshot = onSnapshot(notesCollectionQuery, (querySnapshot) => {
      const notesData = [];
      notesLoaded.value = false;

      querySnapshot.forEach((doc) => {
        const note = {
          id: doc.id,
          content: doc.data().content,
          date: doc.data().date,
        };
        notesData.push(note);
      });

      notes.value = notesData;
      notesLoaded.value = true;
    });
  }

  function clearNotes() {
    notes.value = [];
    if (notesSnapshot) notesSnapshot();
  }

  async function addNewNote(noteContent) {
    const currentDate = new Date().getTime();

    await addDoc(notesCollectionRef, { content: noteContent, date: currentDate });
  }

  async function deleteNote(id) {
    await deleteDoc(doc(notesCollectionRef, id));
  }

  async function updateNote(id, newContent) {
    await updateDoc(doc(notesCollectionRef, id), {
      content: newContent,
    });
  }

  return {
    notes,
    notesLoaded,
    getNoteContentById,
    totalNotesCount,
    totalCharactersCount,
    getNotes,
    clearNotes,
    addNewNote,
    deleteNote,
    updateNote,
  };
});
