import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { defineStore } from "pinia";
import { auth } from "../js/firebase";
import { ref } from "vue";
import { useRouter } from "vue-router";

import { useNotesStore } from "./NotesStore";

export const useAuthStore = defineStore("authStore", () => {
  const user = ref({});
  const router = useRouter();

  const notesStore = useNotesStore();

  function init() {
    onAuthStateChanged(auth, (userDetails) => {
      if (userDetails) {
        user.value = {
          email: userDetails.email,
          uid: userDetails.uid,
        };

        router.push({ name: "Notes" });
        notesStore.getNotes(userDetails.uid);
      } else {
        user.value = {};
        router.replace({ name: "Auth" });
        notesStore.clearNotes();
      }
    });
  }

  function registerUser(credentials) {
    createUserWithEmailAndPassword(auth, credentials.email, credentials.password)
      .then((userCredential) => {
        const user = userCredential.user;
      })
      .catch((error) => {
        const errorMessage = error.message;
        console.log(errorMessage);
      });
  }

  function loginUser(credentials) {
    signInWithEmailAndPassword(auth, credentials.email, credentials.password)
      .then((userCredential) => {
        const user = userCredential.user;
      })
      .catch((error) => {
        const errorMessage = error.message;
        console.log(errorMessage);
      });
  }

  function logoutUser() {
    signOut(auth)
      .then(() => {
        alert("Logout successfully");
      })
      .catch((error) => {
        console.log(error.message);
      });
  }

  return {
    user,
    init,
    registerUser,
    loginUser,
    logoutUser,
  };
});
